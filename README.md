# i3 Configuration files

## Prerequisites

To make everything works as expected some third-party tools has been installed on the system:

### Must have

* `feh` - Wallpapper
* `compton` - Composite manager
* `nm-applet` - Network Manager applet
* `guake`- Drop-down terminal
* `gxkb`- Keyboard layout applet
* `pasystray` - PulseAudio applet
* `pamac-tray` - Pamac updates applet
* `xscreensaver`- Screensaver
* `rofi` - dmenu replacement
* `bumblebee-status` - Status bar

### Not compulsory

* `playerctl` - To controll players
* `ttf-font-awesome` - Font with icons
* `syncthing` - File sync
* `redshift-gtk` - Display color temperature adjuster
* `clipit` - Clipboard manager

### XFCE4 packages
* `xfce4-screenshooter` - For screenshots instead of `scrot`
* `xfce4-power-manager` - Power management
* `xfce4-notifyd` - Notifications

### LXDE packages
* `lxsessions` - For lxpolkit PolicyKit
* `lxappearance` - GTK2/3 config tool

## Bar

To install bumblebee-status just do following

```bash
$ mkdir -p $HOME/build
$ git clone https://github.com/tobi-wan-kenobi/bumblebee-status $HOME/build/bumblebee-status
```
